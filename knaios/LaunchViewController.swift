//
//  LaunchViewController.swift
//  knaios
//
//  Created by Andras Balogh on 10/31/17.
//  Copyright © 2017 iOS Mobile Computing. All rights reserved.
//

import Foundation
import UIKit

class LaunchViewController: UIViewController {
    
    @IBOutlet weak var logo: UIImageView!
    
    fileprivate var originalFrame:CGRect = CGRect()
    fileprivate var originalCenter:CGPoint = CGPoint()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Remember the original frame for the label
        originalFrame = self.logo.frame
        originalCenter = self.logo.center
        self.pulseMe()
        //self.spinMe()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func pulseMe() {
        
        // Calculate the new frame
        let adjust:CGFloat = 20.0
        let originAdjust:CGFloat = adjust / 2
        let sizeAdjust:CGFloat = adjust * 2
        
        let newFrame:CGRect = CGRect(x: originalFrame.origin.x - originAdjust, y: originalFrame.origin.y - originAdjust, width: originalFrame.size.width + sizeAdjust, height: originalFrame.size.height + sizeAdjust)
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.curveLinear, .autoreverse],
                       animations: {
                        self.logo.frame = newFrame
        })
                      /* completion: { finished in
                        if (finished && !self.stopPulsing) {
                            self.pulseMe()
                        }
        }
        )*/
    }
    
    func spinMe() {
        
        UIView.animate(withDuration: 3.0, delay: 0.0, options: [.curveLinear],
                       animations: {
                        self.logo.transform = self.logo.transform.rotated(by: CGFloat(Double.pi))
        })
                      /* completion: { finished in
                        if (finished && !self.stopSpinning) {
                            self.spinMe()
                        }
        }
        ) */
    }
}
