//
//  SignUpViewController.swift
//  knaios
//
//  Created by Andras Balogh on 10/31/17.
//  Copyright © 2017 iOS Mobile Computing. All rights reserved.
//

import Foundation
import UIKit

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var usr: UITextField!
    @IBOutlet weak var pass: UITextField!
    //@IBOutlet weak var emailTxt: UITextField!
    
    var alert = UIAlertController()
    
    @IBAction func signUpButton(_ sender: UIButton) {
        if self.usr.text == "" || self.pass.text == ""{
            self.alert = UIAlertController(title: "Error", message: "Every field must have a value.", preferredStyle: UIAlertControllerStyle.alert)
            let ok = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default) { (action:UIAlertAction) in
            }
            self.alert.addAction(ok)
            self.present(self.alert, animated: true, completion:nil)
        }
        else if (self.usr.text?.contains("."))! {
            self.alert = UIAlertController(title: "Error", message: "Username may not have a '.' in it.", preferredStyle: UIAlertControllerStyle.alert)
            let ok = UIAlertAction(title: "Dimiss", style: UIAlertActionStyle.default) { (action:UIAlertAction) in
            }
            self.alert.addAction(ok)
            self.present(self.alert, animated: true, completion:nil)
        }
        else {
            let user = User(usr: self.usr.text!, pass: self.pass.text!, email: "")
            let userExists = DataStore.shared.userExists(userToAdd: user)
            if userExists {
                self.alert = UIAlertController(title: "Error", message: "That user already exists. Please pick a username that hasn't been chosen yet.", preferredStyle: UIAlertControllerStyle.alert)
                let ok = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default) { (action:UIAlertAction) in
                }
                self.alert.addAction(ok)
                self.present(self.alert, animated: true, completion:nil)
            }
            else {
                DataStore.shared.addPerson(user: user)
                self.alert = UIAlertController(title: "Success", message: "Sucessfully added user.", preferredStyle: UIAlertControllerStyle.alert)
                let ok = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default) { (action:UIAlertAction) in
                }
                self.alert.addAction(ok)
                self.present(self.alert, animated: true, completion:nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.usr.resignFirstResponder()
        self.pass.resignFirstResponder()
        //emailTxt.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
