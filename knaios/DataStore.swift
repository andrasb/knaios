//
//  DataStore.swift
//  mockupPaper
//
//  Created by Nick Aguirre on 10/28/17.
//
//  Majority of code sampled from Canvas.
//  Copyright © 2017 Robert Seitsinger

import Foundation
import Firebase
import FirebaseDatabase

class DataStore {
    
    static let shared = DataStore()
    
    private var ref: DatabaseReference!
    private var people: [User]!
    
    private init() {
        // Get a database reference.
        // Needed before we can read/write to/from the firebase database.
        ref = Database.database().reference()
    }
    
    func count() -> Int {
        return people.count
    }
    
    func getPerson(index: Int) -> User {
        return people[index]
    }
    
    func loadPeople() {
        // Start with an empty array.
        self.people = [User]()
        
        // Fetch the data from Firebase and store it in our internal people array.
        // This is a one-time listener.
        ref.child("user").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get the top-level dictionary.
            let value = snapshot.value as? NSDictionary
            if let users = value {
                // Iterate over the person objects and store in our internal people array.
                for u in users {
                    //                    print(u)
                    let username = u.key as! String
                    let person = u.value as! [String:String]
                    let pass = person["password"]
                    let email = person["email"]
                    let newPerson = User(usr: username, pass: pass!, email: email!)
                    //                    print(newPerson)
                    self.people.append(newPerson)
                }
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func userExists(userToAdd: User) -> Bool{
        //        print(userToAdd.username)
        for users in self.people{
            if users.username == userToAdd.username{
                return true
            }
        }
        return false
    }
    
    func addPerson(user: User) {
        // define array of key/value pairs to store for this person.
        //        self.loadPeople()
        let userRecord = [
            "password": user.password,
            "email": user.email
        ]
        
        // Save to Firebase.
        self.ref.child("user").child(user.username).setValue(userRecord)
        
        // Also save to our internal array, to stay in sync with what's in Firebase.
        people.append(user)
    }
    
    func login(user: User) -> Bool{
        for users in self.people{
            if users.username == user.username && users.password == user.password{
                return true
            }
        }
        return false
    }
    
}

